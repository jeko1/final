package com.example.davaleba43

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomNavigationView = findViewById(R.id.bottomNavigationView)
        val navcontroller = findNavController(R.id.fragmentContainerView)

        bottomNavigationView.setupWithNavController(navcontroller)
    }
}